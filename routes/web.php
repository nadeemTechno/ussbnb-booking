<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});

$router->group(['prefix' => '/api/v1'], function() use ($router) {

    $router->get('/bookings', array('as'=>'Booking List', 'uses'=>'BookingController@bookings'));
    $router->get('/bookings/{id}', array('as'=>'Booking Edit', 'uses'=>'BookingController@getEditBookingDetail'));
    $router->put('/bookings/{id}', array('as'=>'Booking Update', 'uses'=>'BookingController@updateBooking'));
    $router->post('/bookings', array('as'=>'Create Booking', 'uses'=>'BookingController@createBooking'));
    $router->delete('/bookings/{id}', array('as'=>'Delete Booking', 'uses'=>'BookingController@deleteBooking'));
    $router->get('/booking/status/{status}/{id}', array('as'=>'update Booking status', 'uses'=>'BookingController@updateBookingStatus'));

    $router->post('/get/booking/attr', array('as'=>'Booking Search by Attribute', 'uses'=>'BookingController@getBookingByAttr'));
    $router->get('/deleteBooking/{id}', array('as'=>'Delete Booking', 'uses'=>'BookingController@deleteBooking'));

    $router->get('/booking/{id}/details', array('as'=>'Booking Details', 'uses'=>'BookingController@getBookingDetail'));
    $router->get("/bookings/{start}/{end}", array('as'=>'Booking Details', 'uses'=>'BookingController@getBookings'));

});
