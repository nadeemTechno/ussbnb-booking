<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['client_id','guest_id','room_id', 'from', 'till', 'price',
        'status',
        'booking_status_id',
        'comment',
        'seats',
        'price',
        'guest_first_name',
        'guest_last_name',
        'guest_email',
        'guest_company_name',
        'guest_company_reg_nr',
        'guest_company_vat',
        'guest_company_address',
        'guest_company_contacts',
        'checked_in_at'
    ];
}
