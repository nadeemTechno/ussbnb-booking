<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Exception;
use Validator;
class BookingController extends Controller
{
    use ApiResponser;

    public function __construct()
    {

    }
    // get all bookings
    function bookings(){
        try {
            $bookings = Booking::all();

            $data = [
                'success' => true,
                'message' => 'Booking list',
                'code' => Response::HTTP_OK,
                'locale'=>'',
                'data' => [
                    'booking'=> $bookings
                ]
            ];

            return $this->successResponse($data, Response::HTTP_OK);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    //get a single booking details
    function getBookingDetail($id){
        try {
            $booking = Booking::where('id', $id)->first();
            
            $data = [
                'success' => true,
                'message' => 'Booking',
                'code' => Response::HTTP_OK,
                'locale'=>'',
                'data' => [
                    'booking'=> $booking
                ]
            ];

            return $this->successResponse($data, Response::HTTP_ACCEPTED);
        } catch (Exception $e) {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }


    function getBookingByAttr(Request $request)
    {
        try {
            $bookings = Booking::where($request->all())->get();
            return $this->successResponse($bookings, Response::HTTP_CREATED);
        }
        catch (Exception $e) {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }

    //insert new bookings
    function createBooking(Request $request){

        try {
            Log::debug('time', array(0=>$request->all()));
            $start = $request->start_time;
            $end = $request->end_time;

            $from = $request->input('booking_from');
            $till = $request->input('booking_till');

            $start = $this->reFormatTime($start);
            $end = $this->reFormatTime($end);

            $room = $this->checkRoomAvailability($start, $end, $request->input('room_id'));
            Log::debug('time', array(0=>$room));
            if($room) {
                return $this->errorResponse('Room already booked in this interval', Response::HTTP_NOT_ACCEPTABLE);
            }
            $rule = [
                // 'client_id' => 'required|min:1',
                'booking_from' => 'required',
                'booking_till' => 'required',
                'price' => 'required',
                'room_id' 	=> 'required'
            ];

            $validation = Validator::make($request->all(), $rule);
            Log::debug('time', array(0=>$validation->fails()));
            if($validation->fails()){
                $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
                return $this->errorResponse('Validation Fails. ',Response::HTTP_NOT_ACCEPTABLE);
            }
            else{

                $start_time = $from.' '.$start;
                $end_time = $till.' '.$end;
                Log::debug('time', array(0=>$start_time.' '.$end_time));
                $booking = Booking::create([
                    //'client_id' => $request->input('client_id'),
                    'client_id' => 1,
                    'room_id' => $request->input('room_id'),
                    'booking_status_id'=> $request->input('booking_status'),
                    'from' => Carbon::parse($start_time)->format('Y-m-d h:i:s'),
                    'till' => Carbon::parse($end_time)->format('Y-m-d h:i:s'),
                    'comment' 	=> $request->input('comment'),
                    'seats' => $request->input('seat_number'),
                    'price' => $request->input('price'),
                    'guest_first_name' 	=> $request->input('guest_first_name'),
                    'guest_last_name' 	=> $request->input('guest_last_name'),
                    'guest_email' 	=> $request->input('guest_email'),
                    'guest_company_name' 	=> $request->input('guest_company_name'),
                    'guest_company_reg_nr' 	=> $request->input('guest_company_reg_nr'),
                    'guest_company_vat' 	=> $request->input('guest_company_vat'),
                    'guest_company_address' 	=> $request->input('guest_company_address'),
                    'guest_company_contacts' 	=> $request->input('guest_company_contacts'),
                    'checked_in_at' 	=> $request->input('checkedin_at'),
                    'guest_id' 	=> $request->input('guest_id'),
                ]);
                
                $data = [
                    'success' => true,
                    'message' => 'Booking created',
                    'code' => Response::HTTP_CREATED,
                    'locale'=>'',
                    'data' => [
                        'booking'=> $booking
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_CREATED);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    function reFormatTime($time) {

        $time = explode( ' ', $time);

        if($time == "PM") {
            $hours = explode(':', $time);
            $hour = $hours[0] +12;
            return $hour.':'.$hours[1];
        }

        return $time[0];
    }

    //get details booking to edit
    function getEditBookingDetail($id){

        $booking = Booking::where('id', $id)->first();

        $data = [
            'success' => true,
            'message' => 'Booking',
            'code' => Response::HTTP_OK,
            'locale'=>'',
            'data' => [
                'booking'=> $booking
            ]
        ];

        return $this->successResponse($data);
    }

    function updateBooking(Request $request, $id){

        $start = $request->start_time;
        $end = $request->end_time;

        $from = $request->input('booking_from');
        $till = $request->input('booking_till');

        $start = $this->reFormatTime($start);

        $end = $this->reFormatTime($end);
        $start_time = $from.' '.$start;
        $end_time = $till.' '.$end;

        $room = $this->checkRoomAvailability($start, $end, $request->input('room_id'));
        if($room) {
            return $this->errorResponse('Room already booked in this interval', Response::HTTP_NOT_ACCEPTABLE);
        }

        $rule = [
//			'id' 		=> 'required|min:1',
//            //'client_id' => 'required|min:1',
//            'price' 	=> 'required|numeric',
//            'guest_id' 	=> 'required|integer',
//			'room_id' 	=> 'required|min:1'
        ];

        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation Fails.',Response::HTTP_NOT_ACCEPTABLE);
        }
        else{
            try{
                $booking = Booking::find($request->input('booking_id'));

                //$booking->client_id = $request->input('client_id');
                $booking->room_id 	= $request->input('room_id');
                $booking->from 		= $start_time;
                $booking->till 		= $end_time;
                $booking->price 	= $request->input('price');
                $booking->booking_status_id = $request->input('booking_status');
                $booking->comment 	= $request->input('comment');
                $booking->guest_email 	= $request->input('guest_email');
                $booking->guest_first_name 	= $request->input('guest_first_name');
                $booking->guest_last_name 	= $request->input('guest_last_name');
                $booking->guest_company_name 	= $request->input('guest_company_name');
                $booking->guest_company_reg_nr 	= $request->input('guest_company_reg_nr');
                $booking->guest_company_vat 	= $request->input('guest_company_vat');
                $booking->guest_company_address 	= $request->input('guest_company_address');
                $booking->guest_id 	= $request->input('guest_id');
                $booking->checked_in_at 	= $request->input('checked_in_at');

                $booking = $booking->save();
                //$success['booking']  = $booking;
                
                $data = [
                    'success' => true,
                    'message' => 'Booking Updated',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'booking'=> $booking
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_CREATED);
            } catch(\Exception $e) {
                return $this->errorResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    function deleteBooking($id){
        $booking = Booking::find($id)->delete();

        $data = [
            'success' => true,
            'message' => 'Booking deleted',
            'code' => Response::HTTP_OK,
            'locale'=>'',
            'data' => [
                'booking'=> $booking
            ]
        ];
        return $this->successResponse($data, Response::HTTP_OK);
    }

    function updateBookingStatus($status, $id) {
        try {
            ($status == 1) ? $status=2 : $status=1 ;
            $booking = Booking::where('id', $id)->update([
                'booking_status_id'=>$status
            ]);
            return $this->successResponse($booking, Response::HTTP_ACCEPTED);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    function checkRoomAvailability($startDate, $endDate, $room_id ) {

        $booking = Booking::whereBetween('from',[$startDate, $endDate] )
            ->orWhere(function($query) use($startDate, $endDate) {
                $query->where('from','<=',$startDate )
                    ->where('till', '>', $endDate);
            })
            ->where('room_id', $room_id)
            ->get();

        if(count($booking) > 0) {
            return true;
        }
        return false;

    }

    /* Get data using multiple params*/

    public function getBookings($start, $end) {
        $booking = Booking::whereBetween('from',[$start, $end])->get();

        return $this->successResponse($booking, Response::HTTP_ACCEPTED);
    }
}
